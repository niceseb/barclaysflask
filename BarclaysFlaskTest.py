import unittest
from BarclaysFlask import myParisPlot


class MyTestCase(unittest.TestCase):
    def test_plot_weather_data(self):
        self.assertEqual(myParisPlot(), u'https://plot.ly/~SebastianCheung/79')

if __name__ == '__main__':
    unittest.main()
