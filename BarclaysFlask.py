from flask import Flask
from openweathermap_requests import historic_weather_to_df, datetime_to_timestamp
from plotly.graph_objs import *
from plotly.graph_objs import Scatter
import datetime
import logging
import requests
import plotly.plotly as py
import plotly.tools as tls

logger = logging.getLogger()

app = Flask(__name__)


def myParisPlot():
    base_url = 'http://api.openweathermap.org/data/2.5'
    endpoint = '/history/city'
    dt_start = datetime.datetime(2015, 7, 19, 0, 0)
    dt_end = datetime.datetime(2015, 8, 19, 0, 0)
    dt_start = datetime_to_timestamp(dt_start)
    dt_end = datetime_to_timestamp(dt_end)
    params = {
        'q': 'Paris, FR',
        'type': 'hour',
        'start': dt_start,
        'end': dt_end,
    }
    url = base_url + endpoint
    response = requests.get(url, params=params)
    status_code = response.status_code
    if status_code == 200:
        data = response.json()
    else:
        raise (Exception("HTTP status code is %d" % status_code))

    df = historic_weather_to_df(data)

    trace0 = Scatter(
        x=df.index.to_datetime(),
        y=df['main.temp'].values,
        name='temp'
    )

    trace1 = Scatter(
        x=df.index.to_datetime(),
        y=df['main.humidity'].values,

        name='humidity'
    )

    trace2 = Scatter(
        x=df.index.to_datetime(),
        y=df['main.temp_min'].values,

        name='temp_min'
    )

    trace3 = Scatter(
        x=df.index.to_datetime(),
        y=df['main.temp_max'].values,

        name='temp_max'
    )

    data = Data([trace0, trace1, trace2, trace3])

    plot_url = py.plot(data, filename='Paris, France : Hourly Weather Data', auto_open=True)

    return plot_url


@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    myParisPlot()
    app.run()


